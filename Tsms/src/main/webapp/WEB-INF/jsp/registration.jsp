<%-- 
 
    Document   : login
    Created on : Feb 18, 2018, 8:54:08 PM
    Author     : coder
--%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Tsms </title>

    <!-- Bootstrap -->
    <link href="${pageContext.request.contextPath}/resources/vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="${pageContext.request.contextPath}/resources/vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- NProgress -->
    <link href="${pageContext.request.contextPath}/resources/vendors/nprogress/nprogress.css" rel="stylesheet">
   
    <!-- Custom Theme Style -->
    <link href="${pageContext.request.contextPath}/resources/build/css/custom.min.css" rel="stylesheet">
    
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <script src="${pageContext.request.contextPath}/resources/pwstrength.js"></script>
  </head>

  <body class="login">
    <div>
    
     <div class="login_wrapper">
        <div class="animate form login_form">
          <section class="login_content">
                    <form action="/" method="POST" enctype="utf8" >
                <h1>Create Account</h1>
                <div class="form-group row" >
                   
                    <input class="form-control" name="firstName" value="" required="required"  placeholder="First Name"/>
                    <span id="firstNameError" class="alert alert-danger col-sm-4" style="display:none"></span>
                    
                </div>
                <div class="form-group row">
                  
                 <input class="form-control" name="lastName" value="" required="required"  placeholder="Last Name"/>
                    <span id="lastNameError" class="alert alert-danger col-sm-4" style="display:none"></span>
                    
                </div>
                <div class="form-group row">
                   
                    <input type="email" class="form-control" name="email" value="" required="required"  placeholder="Email"/>                   
                    <span id="emailError" class="alert alert-danger col-sm-4" style="display:none"></span>
                    
                </div>
                <div class="form-group row">
                    
                   <input id="password" class="form-control" name="password" value="" type="password" required="required"  placeholder="Password"/>
                    <span id="passwordError" class="alert alert-danger col-sm-4" style="display:none"></span>
                </div>
                <div class="form-group row">
                   
                  <input id="matchPassword" class="form-control" name="matchingPassword" value="" type="password" required="required"  placeholder="Repeat Password"/>
                    <span id="globalError" class="alert alert-danger col-sm-4" style="display:none"></span>
                </div>
                
               
                <br/>
                <div>
                 <button type="submit" class="btn btn-default">submit</button>
                </div>
                 <div class="separator">
                <p class="change_link">Already a member ?
                  <a href="${pageContext.request.contextPath}/login" class="to_register"> Log in </a>
                </p>

                <div class="clearfix"></div>
                <br />
                </div>
               
                
            </form>
          </section>
        </div>

      </div>
    </div>
     <script >

 var serverContext = "${pageContext.request.contextPath}/";

$(document).ready(function () {
	$('form').submit(function(event) {
		register(event);
	});
	
	$(":password").keyup(function(){
		if($("#password").val() != $("#matchPassword").val()){
	        $("#globalError").show().html("Password does not match!");
	    }else{
	    	$("#globalError").html("").hide();
	    }
	});
	
	options = {
		    common: {minChar:8},
		    ui: {
		    	showVerdictsInsideProgressBar:true,
		    	showErrors:true,
		    	errorMessages:{
		    		  wordLength:"Your password is too short",
		    		  wordNotEmail:"Do not use your email as your password",
		    		  wordSequences: "Your password contains sequences",
		    		  wordLowercase: "Use lower case characters",
		    		  wordUppercase: "Use upper case characters",
		    	      wordOneNumber:"Use numbers",
		    		  wordOneSpecialChar:"Use special characters"
		    		}
		    	}
		};
	 $('#password').pwstrength(options);
});

function register(event){
	event.preventDefault();
    $(".alert").html("").hide();
    $(".error-list").html("");
    if($("#password").val() != $("#matchPassword").val()){
    	$("#globalError").show().html("Password does not match!");
    	return;
    }
    var formData= $('form').serialize();
    $.post(serverContext + "user/registration",formData ,function(data){
        if(data.message == "success"){
            window.location.href = serverContext + "successRegister";
        }
        
    })
    .fail(function(data) {
        if(data.responseJSON.error.indexOf("MailError") > -1)
        {
            window.location.href = serverContext + "emailError.html";
        }
        else if(data.responseJSON.error == "UserAlreadyExist"){
            $("#emailError").show().html(data.responseJSON.message);
        }
        else if(data.responseJSON.error.indexOf("InternalError") > -1){
            window.location.href = serverContext + "login?message=" + data.responseJSON.message;
        }
        else{
        	var errors = $.parseJSON(data.responseJSON.message);
            $.each( errors, function( index,item ){
            	if (item.field){
            		$("#"+item.field+"Error").show().append(item.defaultMessage+"<br/>");
            	}
            	else {
            		$("#globalError").show().append(item.defaultMessage+"<br/>");
            	}
               
            });
        }
    });
}


</script>
    
   
    
  </body>
</html>