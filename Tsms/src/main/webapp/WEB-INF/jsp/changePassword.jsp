<%-- 
 
    Document   : login
    Created on : Feb 18, 2018, 8:54:08 PM
    Author     : coder
--%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Tsms </title>

    <!-- Bootstrap -->
    <link href="${pageContext.request.contextPath}/resources/vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="${pageContext.request.contextPath}/resources/vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- NProgress -->
    <link href="${pageContext.request.contextPath}/resources/vendors/nprogress/nprogress.css" rel="stylesheet">
   
    <!-- Custom Theme Style -->
    <link href="${pageContext.request.contextPath}/resources/build/css/custom.min.css" rel="stylesheet">
    
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <script src="${pageContext.request.contextPath}/resources/pwstrength.js"></script>
  </head>

  <body class="login">
    <div>
    
     <div class="login_wrapper">
        <div class="animate form login_form">
          <section class="login_content">
                    <form  >
                <h1>Update Password</h1>
                
                <div class="form-group row">
                 
                    
                  <input id="oldpass" class="form-control" name="oldPassword" value="" type="password" required="required"  placeholder="Old"/>
                    <span id="passwordError" class="alert alert-danger col-sm-4" style="display:none"></span>
                    
                </div>
                <div class="form-group row">
                    
                   <input id="password" class="form-control" name="newPassword" value="" type="password" required="required"  placeholder="Password"/>
                    <span id="passwordError" class="alert alert-danger col-sm-4" style="display:none"></span>
                </div>
                <div class="form-group row">
                   
                  <input id="matchPassword" class="form-control" name="matchingPassword" value="" type="password" required="required"  placeholder="Repeat Password"/>
                    <span id="globalError" class="alert alert-danger col-sm-4" style="display:none"></span>
                </div>
                
               
                <br/>
                <div>
                 <button onclick="savePass()" type="submit" class="btn btn-default">Update</button>
                </div>
          
               
                
            </form>
          </section>
        </div>

      </div>
    </div>
    
     <script >

 var serverContext = "${pageContext.request.contextPath}/";

$(document).ready(function () {
	$('form').submit(function(event) {
		register(event);
	});
	
	$(":password").keyup(function(){
		if($("#password").val() != $("#matchPassword").val()){
	        $("#globalError").show().html("Password does not match!");
	    }else{
	    	$("#globalError").html("").hide();
	    }
	});
	
	options = {
		    common: {minChar:8},
		    ui: {
		    	showVerdictsInsideProgressBar:true,
		    	showErrors:true,
		    	errorMessages:{
		    		  wordLength:"Your password is too short",
		    		  wordNotEmail:"Do not use your email as your password",
		    		  wordSequences: "Your password contains sequences",
		    		  wordLowercase: "Use lower case characters",
		    		  wordUppercase: "Use upper case characters",
		    	      wordOneNumber:"Use numbers",
		    		  wordOneSpecialChar:"Use special characters"
		    		}
		    	}
		};
	 $('#password').pwstrength(options);
});

function savePass(){
    event.preventDefault();
    $(".alert").html("").hide();
    $(".error-list").html("");
    if($("#password").val() != $("#matchPassword").val()){
        $("#globalError").show().html("password dont Match");
        return;
    }
    var formData= $('form').serialize();
    $.post(serverContext + "user/updatePassword",formData ,function(data){
    	window.location.href = serverContext + "index.html" + "?message="+data.message;
    })
    .fail(function(data) {
        if(data.responseJSON.error.indexOf("InvalidOldPassword") > -1) {
            $("#errormsg").show().append(data.responseJSON.message);
        }
        else if(data.responseJSON.error.indexOf("InternalError") > -1){
            $("#errormsg").show().append(data.responseJSON.message);
        }
        else{
        	var errors = $.parseJSON(data.responseJSON.message);
            $.each( errors, function( index,item ){
                $("#globalError").show().html(item.defaultMessage);
            });
            errors = $.parseJSON(data.responseJSON.error);
            $.each( errors, function( index,item ){
                $("#globalError").show().append(item.defaultMessage+"<br/>");
            });
        }
    });
}

</script>
    
   
    
  </body>
</html>