<%-- 
 <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <script src="${pageContext.request.contextPath}/resources/pwstrength.js"></script>
    Document   : login
    Created on : Feb 18, 2018, 8:54:08 PM
    Author     : coder
--%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Tsms </title>

    <!-- Bootstrap -->
    <link href="${pageContext.request.contextPath}/resources/vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="${pageContext.request.contextPath}/resources/vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- NProgress -->
    <link href="${pageContext.request.contextPath}/resources/vendors/nprogress/nprogress.css" rel="stylesheet">
   
    <!-- Custom Theme Style -->
    <link href="${pageContext.request.contextPath}/resources/build/css/custom.min.css" rel="stylesheet">
    
    
  </head>

  <body class="login">
    <div>
    
     <div class="login_wrapper">
        <div class="animate form login_form">
          <section class="login_content">
           <form name='f' action="login" method='POST' onsubmit="return validate();">
                            
                            <h1>Login Form</h1>
                            <div>
                                <input  name='username' type="text" class="form-control" placeholder="Username" required="" />
                            </div>
                            <div>
                                <input name='password' type="password" class="form-control" placeholder="Password" required="" />
                            </div>

                            <div>
                            <button  type="submit" class="btn btn-default">Login</button>

                               
                            </div>


                            <div class="clearfix"></div>

                            <div class="separator">
                                <p class="change_link">Forget Password?
                                    <a href="${pageContext.request.contextPath}/registration" class="to_register"> Create Account </a>
                                </p>

                                <div class="clearfix"></div>
                                <br />

                            </div>
                            
                        </form>
          </section>
        </div>

      </div>
    </div>
    
   
    
  </body>
</html>