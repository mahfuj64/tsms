package com.mahfuj.persistence.dao;

import java.security.Principal;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;

import com.mahfuj.persistence.model.Staff;

@Repository
public  class StaffDaoImp implements StaffDao {
	
	@PersistenceContext
    private EntityManager entityManager;

	@Override
	public void addStaff(Staff staff) {
		  entityManager.persist(staff);
		
	}

	@Override
	public void update(Staff staff) {
		entityManager.merge(staff);
	}

	@Override
	@SuppressWarnings("unchecked")
	public List<Staff> getAllStaffs(Principal principal) {

        return entityManager.createQuery("from Staff where adder= '"+ principal.getName()+"' ").getResultList();
                
	
	}

	@Override
	public Staff getStaffById(Integer staffid) {
		return entityManager.find(Staff.class,staffid);
	}

	@Override
	public void delete(Staff staff) {
		entityManager.remove(staff);
		
	}

}
