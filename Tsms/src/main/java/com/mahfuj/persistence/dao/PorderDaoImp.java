package com.mahfuj.persistence.dao;

import java.security.Principal;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;

import com.mahfuj.persistence.model.Porder;

@Repository
public class PorderDaoImp implements PorderDao {
	  @PersistenceContext
	    private EntityManager entityManager;
	 
	 
  @Override
  public void addPorder(Porder porder) {
      entityManager.persist(porder);

  }

  @SuppressWarnings("unchecked")
  @Override
  public List<Porder> getAllPorders(Principal principal) {
  	

      return entityManager.createQuery("from Porder where adder= '"+ principal.getName()+"' ").getResultList();
              
  }


	@Override
	public Porder getPorderById(Integer porderid) {
		
		//List<Porder>list=entityManager.createQuery("from Porder c where c.id=:Porderid").setParameter("Porderid",Porderid).getResultList();
	
  	//return list.size()>0?(Porder)list.get(0):null;
  	
  	return entityManager.find(Porder.class,porderid);
  }

	@Override
	public void update(Porder porder) {
	entityManager.merge(porder);
	
	
	}

	@Override
	public void delete(Porder porder) {
		entityManager.remove(porder);
		
	}
}
