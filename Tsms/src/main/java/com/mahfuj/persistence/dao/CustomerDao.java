/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mahfuj.persistence.dao;

import com.mahfuj.persistence.model.Customer;

import java.security.Principal;
import java.util.List;

/**
 *
 * @author coder
 */
public interface CustomerDao {
    
    
    public void addCustomer(Customer customer);
    public void update(Customer customer);
    public List<Customer> getAllCustomers(Principal principal);
    public Customer getCustomerById(Integer customerid);

	
 
    
}
