package com.mahfuj.persistence.dao;

import java.security.Principal;
import java.util.List;

import com.mahfuj.persistence.model.Porder;

public interface IndexDao {
	 public List<Porder> getReady(Principal principal);
	 public List<Porder> getConfirm(Principal principal);
	 public List<Porder> getDue(Principal principal);
	 public Object getTotalAmout(Principal principal);
	 public Object getTotalPaid(Principal principal);
	public List<Porder> getDelivered(Principal principal);

}
