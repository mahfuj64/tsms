package com.mahfuj.persistence.dao;

import java.security.Principal;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.stereotype.Repository;

import com.mahfuj.persistence.model.Porder;

@Repository
public class IndexDaoImp implements IndexDao {
	
	
	 @PersistenceContext
	    private EntityManager entityManager;

	@SuppressWarnings("unchecked")
	@Override
	public List<Porder> getReady(Principal principal) {
		 return entityManager.createQuery("from Porder where adder= '"+ principal.getName()+"' and status='Ready' ").getResultList();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Porder> getConfirm(Principal principal) {
		 return entityManager.createQuery("from Porder where adder= '"+ principal.getName()+"' and status='Confirm' ").getResultList();
			}
	
	@Override
	@SuppressWarnings("unchecked")
	public List<Porder> getDue(Principal principal) {
		 return entityManager.createQuery("from Porder where adder= '"+ principal.getName()+"' and status='Processing' ").getResultList();
	}

	@Override
	
	public Object getTotalAmout(Principal principal) {
	Query q= entityManager.createQuery("SELECT SUM(amount) from Porder where adder= '"+ principal.getName()+"' ");
	Object result =  q.getSingleResult();
      return  result; 
	}

	@Override
	
	public Object getTotalPaid(Principal principal) {
		Query q= entityManager.createQuery("SELECT SUM(paid) from Porder where adder= '"+ principal.getName()+"' ");
		 Object result = q.getSingleResult ();
	      return  result; 
	}

	@Override
	@SuppressWarnings("unchecked")
	public List<Porder> getDelivered(Principal principal) {
		return entityManager.createQuery("from Porder where adder= '"+ principal.getName()+"' and status='Delivered' ").getResultList();
	}
}
