package com.mahfuj.persistence.dao;

import java.security.Principal;
import java.util.List;

import com.mahfuj.persistence.model.Salary;

public interface SalaryDao {
	
	    public void addSalary(Salary salary);
	    public void update(Salary salary);
	    public Salary getSalaryById(Integer salaryid);
	    public List<Salary> getAllSalarys(Principal principal);


}
