package com.mahfuj.persistence.dao;

import java.security.Principal;
import java.util.List;

import com.mahfuj.persistence.model.Staff;

public interface StaffDao {
	  
    public void addStaff(Staff staff);
    public void update(Staff staff);
    public List<Staff> getAllStaffs(Principal principal);
    public Staff getStaffById(Integer staffid);
	public void delete(Staff staff);

}
