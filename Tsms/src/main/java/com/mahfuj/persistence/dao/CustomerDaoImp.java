/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mahfuj.persistence.dao;

import com.mahfuj.persistence.model.Customer;

import java.security.Principal;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 *
 * @author coder
 */

@Repository
public class CustomerDaoImp implements CustomerDao{
    
	  @PersistenceContext
	    private EntityManager entityManager;
	 
	 
    @Override
    public void addCustomer(Customer customer) {
        entityManager.persist(customer);
 
    }
 
    @SuppressWarnings("unchecked")
    @Override
    public List<Customer> getAllCustomers(Principal principal) {
    	
 
        return entityManager.createQuery("from Customer where adder= '"+ principal.getName()+"' ").getResultList();
                
    }

  
	@Override
	public Customer getCustomerById(Integer customerid) {
		
		//List<Customer>list=entityManager.createQuery("from Customer c where c.id=:customerid").setParameter("customerid",customerid).getResultList();
	
    	//return list.size()>0?(Customer)list.get(0):null;
    	
    	return entityManager.find(Customer.class,customerid);
    }

	@Override
	public void update(Customer customer) {
	entityManager.merge(customer);
	
	
	}
 
   
    }
 
    
    

