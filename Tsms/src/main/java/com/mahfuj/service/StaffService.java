package com.mahfuj.service;

import java.security.Principal;
import java.util.List;

import com.mahfuj.persistence.model.Staff;;

public interface StaffService {
	  
    public void addStaff(Staff staff);
    public void update(Staff staff);
    public void delete(Staff staff);
    public Staff getStaffById(Integer staffid);
    public List<Staff> getAllStaffs(Principal principal);

}
