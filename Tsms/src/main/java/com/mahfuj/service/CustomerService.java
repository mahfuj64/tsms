/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mahfuj.service;


import com.mahfuj.persistence.model.Customer;

import java.security.Principal;
import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author coder
 */
@Service
@Transactional
public interface CustomerService {
    
    
    public void addCustomer(Customer customer);
    public void update(Customer customer);
    public Customer getCustomerById(Integer customerid);
    public List<Customer> getAllCustomers(Principal principal);
 
    
}
