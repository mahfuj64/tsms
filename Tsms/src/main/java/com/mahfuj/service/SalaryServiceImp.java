package com.mahfuj.service;

import java.security.Principal;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.mahfuj.persistence.dao.SalaryDao;
import com.mahfuj.persistence.model.Salary;

@Service
@Transactional
public class SalaryServiceImp implements SalaryService {

	    @Autowired
		private SalaryDao salaryDao;

		@Override
		@Transactional
		public void addSalary(Salary salary) {
		
		salaryDao.addSalary(salary);	
		}

		@Override
		@Transactional
		public void update(Salary salary) {
			salaryDao.update(salary);
			
		}

		@Override
		@Transactional
		public Salary getSalaryById(Integer salaryid) {
			// TODO Auto-generated method stub
			return salaryDao.getSalaryById(salaryid);
		}

		@Override
		@Transactional
		public List<Salary> getAllSalarys(Principal principal) {
			// TODO Auto-generated method stub
			return salaryDao.getAllSalarys(principal);
		}

		
		

}
