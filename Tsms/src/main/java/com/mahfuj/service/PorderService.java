package com.mahfuj.service;

import java.security.Principal;
import java.util.List;

import com.mahfuj.persistence.model.Porder;
import com.mahfuj.persistence.model.Staff;

public interface PorderService {
	    public void addPorder(Porder porder);
	    public void update(Porder porder);
	    public Porder getPorderById(Integer porderid);
	    public List<Porder> getAllPorders(Principal principal);
	    public void delete(Porder porder);

}
