/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mahfuj.service;

import com.mahfuj.persistence.model.Customer;

import java.security.Principal;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.mahfuj.persistence.dao.CustomerDao;

/**
 *
 * @author coder
 */
@Service
@Transactional
public class CustomerServiceImp implements CustomerService{
    
    @Autowired
    private CustomerDao customerDao;
 
    @Override
    @Transactional
    public void addCustomer(Customer Customer) {
        customerDao.addCustomer(Customer);
    }
 
    @Override
    @Transactional
    public List<Customer> getAllCustomers(Principal principal) {
        return customerDao.getAllCustomers(principal);
    }

	@Override
	 @Transactional
	public Customer getCustomerById(Integer customerid) {
		// TODO Auto-generated method stub
		return customerDao.getCustomerById(customerid);
	}

	@Override
	 @Transactional
	public void update(Customer customer) {
		customerDao.update(customer);
		
	}
 
 
  
    
}
