package com.mahfuj.service;

import java.security.Principal;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.mahfuj.persistence.dao.PorderDao;
import com.mahfuj.persistence.model.Porder;


@Service
@Transactional
public class PorderServiceImp implements PorderService {
	
	@Autowired
	private PorderDao porderDao;

	 @Override
	    @Transactional
	    public void addPorder(Porder porder) {
	         porderDao.addPorder(porder);
	    }
	 
	    @Override
	    @Transactional
	    public List<Porder> getAllPorders(Principal principal) {
	        return  porderDao.getAllPorders(principal);
	    }

		@Override
		 @Transactional
		public Porder getPorderById(Integer porderid) {
			// TODO Auto-generated method stub
			return  porderDao.getPorderById(porderid);
		}

		@Override
		 @Transactional
		public void update(Porder porder) {
			 porderDao.update(porder);
			
		}

		@Override
		public void delete(Porder porder) {
			// TODO Auto-generated method stub
			porderDao.delete(porder);
			
		}
	 
}
