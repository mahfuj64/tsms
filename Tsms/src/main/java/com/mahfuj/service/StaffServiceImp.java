package com.mahfuj.service;

import java.security.Principal;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.mahfuj.persistence.dao.StaffDao;
import com.mahfuj.persistence.model.Staff;

@Service
@Transactional
public class StaffServiceImp implements StaffService {
	
	 @Autowired
	private StaffDao staffDao;

	@Override
	@Transactional
	public void addStaff(Staff staff) {
	
	staffDao.addStaff(staff);	
	}

	@Override
	@Transactional
	public void update(Staff staff) {
		staffDao.update(staff);
		
	}

	@Override
	@Transactional
	public Staff getStaffById(Integer staffid) {
		// TODO Auto-generated method stub
		return staffDao.getStaffById(staffid);
	}

	@Override
	@Transactional
	public List<Staff> getAllStaffs(Principal principal) {
		// TODO Auto-generated method stub
		return staffDao.getAllStaffs(principal);
	}

	@Override
	public void delete(Staff staff) {
		 staffDao.delete(staff);
		
	}
	
	

}
