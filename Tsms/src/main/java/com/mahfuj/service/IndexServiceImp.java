package com.mahfuj.service;

import java.security.Principal;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.mahfuj.persistence.dao.IndexDao;
import com.mahfuj.persistence.model.Porder;

@Service
@Transactional
public class IndexServiceImp implements IndexService {
	
	@Autowired
	private IndexDao indexDao;

	@Override
	@Transactional
	public List<Porder> getReady(Principal principal) {
		// TODO Auto-generated method stub
		return indexDao.getReady(principal);
	}

	@Override
	@Transactional
	public List<Porder> getConfirm(Principal principal) {
		// TODO Auto-generated method stub
		return indexDao.getConfirm(principal);
	}

	@Override
	@Transactional
	public List<Porder> getDue(Principal principal) {
		// TODO Auto-generated method stub
		return indexDao.getDue(principal);
	}

	@Override
	@Transactional
	public Object getTotalAmout(Principal principal) {
		// TODO Auto-generated method stub
		return indexDao.getTotalAmout(principal);
	}

	@Override
	@Transactional
	public Object getTotalPaid(Principal principal) {
		// TODO Auto-generated method stub
		return indexDao.getTotalPaid(principal);
	}

	@Override
	@Transactional
	public List<Porder> getDelivered(Principal principal) {
		return indexDao.getDelivered(principal);
	}

}
