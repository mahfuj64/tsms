/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mahfuj.web.controller;


import com.mahfuj.persistence.model.Customer;
import org.springframework.stereotype.Controller;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.h2.util.New;
import org.jboss.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import com.mahfuj.service.CustomerService;
import com.twilio.sdk.TwilioRestClient;
import com.twilio.sdk.TwilioRestException;
import com.twilio.sdk.resource.factory.MessageFactory;
import com.twilio.sdk.resource.instance.Message;

import java.io.IOException;
import java.security.Principal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;


@Controller
public class CustomerController {
 
    private static final Logger logger = Logger
            .getLogger(CustomerController.class);
 
    public CustomerController() {
        System.out.println("CustomerController()");
    }
 
    @Autowired
    private CustomerService customerService;
    
    
    public static final String ACCOUNT_SID = "ACe8a38083a5a012172cf28f1c5d0b679f";
    public static final String AUTH_TOKEN = "d8de3ad2c8d44d3353079fb04ee283ad";
    public static final String TWILIO_NUMBER = "+16024617573";
    
    
    
     @RequestMapping(value = "/customer/add", method = RequestMethod.POST)
    public String saveCustomer(@ModelAttribute("customer")Customer customer, @RequestParam("phone") String phone,BindingResult result,Principal principal) {
        if (customer.getId() == null) {
            customer.setAdder(principal.getName());
            customerService.addCustomer(customer);
            
            try {
                TwilioRestClient client = new TwilioRestClient(ACCOUNT_SID, AUTH_TOKEN);
         
                List<NameValuePair> params = new ArrayList<NameValuePair>();
                params.add(new BasicNameValuePair("Body", "Thank You For Join Us"));
                params.add(new BasicNameValuePair("To", phone)); 
                params.add(new BasicNameValuePair("From", TWILIO_NUMBER));
         
                MessageFactory messageFactory = client.getAccount().getMessageFactory();
                Message message = messageFactory.create(params);
                System.out.println(message.getSid());
            } 
            catch (TwilioRestException e) {
                System.out.println(e.getErrorMessage());
            }
            
        } 
       
       
        return "redirect:/customerlist.html";
    }
     
     @RequestMapping(value = "/customerlist.html", method = RequestMethod.GET)
     public String customerList(Map<String, Object> map,Principal principal) {
    	 
    	 map.put("listcustomer", new Customer());
    	 map.put("customerlist",customerService.getAllCustomers(principal));
    	 
    	 return "customerlist";
     }
     
     @RequestMapping(value = "/editCustomer/{id}", method = RequestMethod.GET)
     public String editCustomer(@PathVariable("id")Integer id, Map<String, Object> map,Principal principal){
    	 Customer customer=customerService.getCustomerById(id);
    	 map.put("customer",customer);
    	
    	 return "editCustomer";
     }
   
     
     @RequestMapping(value = "/customer/update", method = RequestMethod.POST)
     public String updateCustomer(@ModelAttribute("customer")Customer customer,BindingResult result) {
    	 if (customer.getId() != null) {
    		Customer custom=customerService.getCustomerById(customer.getId());
    		
    		 
    	    customer.setAdder(custom.getAdder());
    	    customer.setGender(custom.getGender());
    	    
 			customerService.update(customer);
    	 }
         return "redirect:/customerlist.html";
     }
     
     @RequestMapping(value = "/Customerprofile/{id}", method = RequestMethod.GET)
     public String viewCustomer(@PathVariable("id")Integer id, Map<String, Object> map){
    	 Customer customer=customerService.getCustomerById(id);
    	 map.put("id", customer.getId());
    	 map.put("name", customer.getName());
    	 map.put("phone", customer.getPhone());
    	 map.put("email", customer.getEmail());
    	 map.put("adress", customer.getAdress());
    	 map.put("Gender", customer.getGender());
    	
    	 return "Customerprofile";
     }
     

   
}
