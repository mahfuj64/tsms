/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mahfuj.web.controller;


import java.security.Principal;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.mahfuj.persistence.model.Customer;
import com.mahfuj.persistence.model.Staff;
import com.mahfuj.service.CustomerService;
import com.mahfuj.service.IndexService;
import com.mahfuj.service.PorderService;
import com.mahfuj.service.SalaryService;
import com.mahfuj.service.StaffService;







@org.springframework.stereotype.Controller
public class Controller {
	
	
	
	
	 @Autowired
	 private StaffService StaffService;
	 
	  @Autowired
	  private CustomerService customerService;
	  
	  @Autowired
	   private PorderService porderService;
	  
	  
	  @Autowired
	  private SalaryService salaryService;
	  
	@Autowired
	private  IndexService indexService;
	    
    
  @RequestMapping(value = "/index.html",method = RequestMethod.GET)
    public String indexre(Map<String, Object> map,Principal principal){
	  
	  int cus=customerService.getAllCustomers(principal).size();
	  
	  map.put("customer", cus);
	  
	  int staff=StaffService.getAllStaffs(principal).size();
	  map.put("Staff", staff);
    
	int order=  porderService.getAllPorders(principal).size();
	
	map.put("order", order);
	
	int orderready=indexService.getReady(principal).size();
	map.put("orderready",orderready);
			
    int orderconfirm=indexService.getConfirm(principal).size();
    map.put("orderconfirm", orderconfirm);
    
    int orderdue=indexService.getDue(principal).size();
    map.put("orderdue", orderdue);
    
    Object amount=indexService.getTotalAmout(principal);
    map.put("amount", amount);
    Object paid=indexService.getTotalPaid(principal);
    map.put("paid", paid);
    
    int orderdelivered=indexService.getDelivered(principal).size();
    map.put("orderdelivered", orderdelivered);
    
        return  "index";
    }
 
  
  
    @RequestMapping(value = "/addcustomer.html",method = RequestMethod.GET)
    public String Customer(){
        
        
        
    return "addcustomer";
    }

  
    @RequestMapping(value = "/addstaff.html",method = RequestMethod.GET)
    public String Staff(){
        
        
        
    return "addstaff";
    }
    
    @RequestMapping(value = "/addsalary.html",method = RequestMethod.GET)
    public String salary(Map<String, Object> map,Principal principal){
        
    	   
    	   	 map.put("listStaff", new Staff());
    	   	 map.put("Stafflist",StaffService.getAllStaffs(principal));
        
    return "addsalary";
    }
       
    
 
    

    
    @RequestMapping(value = "/profile",method = RequestMethod.GET)
    public String profile(){
        
        
        
    return "profile";
    }
  
    @RequestMapping(value = "/addorder/{id}", method = RequestMethod.GET)
    public String addorder(@PathVariable("id")Integer id,Map<String, Object> map,Principal principal) {
   	 
   	 map.put("listStaff", new Staff());
   	 map.put("Stafflist",StaffService.getAllStaffs(principal));
   	 
   	 Customer customer=customerService.getCustomerById(id);
	 map.put("id", customer.getId());
	 map.put("name", customer.getName());
	 map.put("phone", customer.getPhone());
	 map.put("email", customer.getEmail());
	 map.put("adress", customer.getAdress());
	 map.put("Gender", customer.getGender());
   	 
   	 return "addorder";
    }
    
   
    
      
 
}
