package com.mahfuj.web.controller;

import java.security.Principal;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Map;

import org.jboss.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.mahfuj.persistence.model.Porder;

import com.mahfuj.service.PorderService;

@Controller
public class PorderController {
 
    private static final Logger logger = Logger
            .getLogger(PorderController.class);
 
    public PorderController() {
        System.out.println("PorderController()");
    }
 
    @Autowired
    private PorderService porderService;
    
    
    LocalDateTime now = LocalDateTime.now(); 
    DateTimeFormatter format = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm:ss");  
    String formatDateTime = now.format(format);  
 
    
    
     @RequestMapping(value = "/porder/add", method = RequestMethod.POST)
    public String savePorder(@ModelAttribute("Porder")Porder porder,BindingResult result,Principal principal) {
    	 
    	 if (porder.getId() == null) {
        porder.setAdder(principal.getName());
        porder.setDate(formatDateTime);
        porderService.addPorder(porder);
        } 
       
        return "redirect:/orderlist.html";
    }
     
     @RequestMapping(value = "/orderlist.html", method = RequestMethod.GET)
     public String PorderList(Map<String, Object> map,Principal principal) {
    	 
    	 map.put("listPorder", new Porder());
    	 map.put("Porderlist",porderService.getAllPorders(principal));
    	 
    	 return "orderlist";
     }
     
     @RequestMapping(value = "/addmeasurement/{id}", method = RequestMethod.GET)
     public String editPorder(@PathVariable("id")Integer id, Map<String, Object> map,Principal principal){
    	Porder porder=porderService.getPorderById(id);
    	 map.put("porder",porder);
    	
    	 return "m";
     }
     
     @RequestMapping(value = "/porder/update", method = RequestMethod.POST)
     public String updatePorder(@ModelAttribute("Porder")Porder porder,BindingResult result) {
    	 if (porder.getId() != null) {
    		Porder order=porderService.getPorderById(porder.getId());

    		 
    	    porder.setCustomername(order.getCustomername());
    		porder.setStaffname(order.getStaffname());
    		porder.setCustomerphone(order.getCustomerphone());
    		porder.setAmount(order.getAmount());
    		porder.setPaid(order.getPaid());
    		porder.setOrderdetails(order.getOrderdetails());
    		porder.setDeliverydate(order.getDeliverydate());
    		
    		porder.setDate(order.getDate());
    	    porder.setAdder(order.getAdder());
    	   
    	    
 			porderService.update(porder);
    	 }
         return "redirect:/orderlist.html";
     }
     
     @RequestMapping(value = "/porderview/{id}", method = RequestMethod.GET)
     public String viewPorder(@PathVariable("id")Integer id, Map<String, Object> map){
    	 Porder order=porderService.getPorderById(id);
    	 map.put("id", order.getId());
    	 map.put("customername", order.getCustomername());
    	 map.put("customerphone", order.getCustomerphone());
    	 map.put("staffname",order.getStaffname());
    	 map.put("amount", order.getAmount());
    	 map.put("paid", order.getPaid());
    	 map.put("status",order.getStatus());
    	 map.put("orderdetails",order.getOrderdetails());
    	 map.put("delyverdate", order.getDeliverydate());
    	 map.put("curentdate", formatDateTime);
    	 map.put("Orderdate", order.getDate());
    	 map.put("outseam",order.getOutseam());
    	 map.put("inseam",order.getInseam());
    	 map.put("ankles",order.getAnkles());
    	 map.put("crotch",order.getCrotch());
    	 map.put("thigh",order.getThigh());
    	 map.put("hips",order.getHips());
    	 map.put("waist",order.getWaist());
    	 map.put("chest",order.getChest());
    	 map.put("shoulder",order.getShoulder() );
    	 map.put("length",order.getLength());
    	 map.put("wrist",order.getWrist());
    	 map.put("bicep",order.getBicep());
    	 map.put("sleeve",order.getSleeve());
    	 map.put("neck",order.getNeck());
    	 map.put("other",order.getOther());
    
    	
    	 
    	
    	 return "invoice";
     }
     @RequestMapping(value = "/deleteorder/{id}", method = RequestMethod.GET)
     public String deletePorder(@PathVariable("id")Integer id, Map<String, Object> map){
    	 Porder Porder=porderService.getPorderById(id);
         porderService.delete(Porder);    	
    	 return "redirect:/orderlist.html";
     }
     
     
    

   
}
