/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mahfuj.web.controller;


import com.mahfuj.persistence.model.Salary;
import com.mahfuj.persistence.model.Salary;
import org.springframework.stereotype.Controller;
import org.h2.util.New;
import org.jboss.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import com.mahfuj.service.SalaryService;
import java.io.IOException;
import java.security.Principal;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Map;

import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;


@Controller
public class SalaryController {
 
    private static final Logger logger = Logger
            .getLogger(SalaryController.class);
 
    public SalaryController() {
        System.out.println("SalaryController()");
    }
 
    @Autowired
    private SalaryService salaryService;
    
    
    LocalDateTime now = LocalDateTime.now(); 
  DateTimeFormatter format = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm:ss");  
  String formatDateTime = now.format(format);  
 
    
    
     @RequestMapping(value = "/salary/add", method = RequestMethod.POST)
    public String saveSalary(@ModelAttribute("Salary")Salary salary,BindingResult result,Principal principal) {
        if (salary.getId() == null) {
            salary.setDate(formatDateTime);
            salary.setAdder(principal.getName());
            salaryService.addSalary(salary);
        } 
       
        return "redirect:/salarylist.html";
    }
     
     @RequestMapping(value = "/salarylist.html",method = RequestMethod.GET)
     public String salaryList(Map<String, Object> map,Principal principal){
         
    	 map.put("listsalary", new Salary());
    	 map.put("salarylist",salaryService.getAllSalarys(principal));
    	 
    	
         
     return "salarylist";
     }
     
     @RequestMapping(value = "/salaryinvoice/{id}", method = RequestMethod.GET)
     public String viewSalary(@PathVariable("id")Integer id, Map<String, Object> map){
    	 Salary salary=salaryService.getSalaryById(id);
    	 map.put("id", salary.getId());
    	 map.put("sname", salary.getSname());
    	 map.put("phone", salary.getPhone());
    	 map.put("month", salary.getMonth());
    	 map.put("paid", salary.getPaidsalary());
    	 map.put("date", salary.getDate());
    	
    	 return "salaryinvoice";
     }
     
   

   
}
