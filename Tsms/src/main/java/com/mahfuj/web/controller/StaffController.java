package com.mahfuj.web.controller;

import java.security.Principal;
import java.util.Map;

import org.jboss.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.mahfuj.persistence.model.Staff;
import com.mahfuj.service.StaffService;

@Controller
public class StaffController {
	
	private static final Logger logger = Logger
            .getLogger(StaffController.class);
 
    public StaffController() {
        System.out.println("StaffController()");
    }
 
    @Autowired
    private StaffService StaffService;
    
    
 
    
    
     @RequestMapping(value = "/staff/add", method = RequestMethod.POST)
    public String saveStaff(@ModelAttribute("Staff")Staff staff,BindingResult result,Principal principal) {
        if (staff.getId() == null) {
       staff.setAdder(principal.getName());
            StaffService.addStaff(staff);
        } 
       
        return "redirect:/stafflist.html";
    }
     
     @RequestMapping(value = "/stafflist.html", method = RequestMethod.GET)
     public String StaffList(Map<String, Object> map,Principal principal) {
    	 
    	 map.put("listStaff", new Staff());
    	 map.put("Stafflist",StaffService.getAllStaffs(principal));
    	 
    	 return "stafflist";
     }
     
     
   
     
     @RequestMapping(value = "/editStaff/{id}", method = RequestMethod.GET)
     public String editStaff(@PathVariable("id")Integer id, Map<String, Object> map,Principal principal){
    	 Staff Staff=StaffService.getStaffById(id);
    	 map.put("staff",Staff);
    	
    	 return "editstaff";
     }
     
     @RequestMapping(value = "/Staff/update", method = RequestMethod.POST)
     public String updateStaff(@ModelAttribute("staff")Staff staff,BindingResult result) {
    	 if (staff.getId() != null) {
    		Staff staf=StaffService.getStaffById(staff.getId());
    		
    		 
    	    staff.setAdder(staf.getAdder());
    	    staff.setGender(staf.getGender());
    	    
 			StaffService.update(staff);
    	 }
         return "redirect:/stafflist.html";
     }
     @RequestMapping(value = "/Staffprofile/{id}", method = RequestMethod.GET)
     public String viewStaff(@PathVariable("id")Integer id, Map<String, Object> map){
    	 Staff staff=StaffService.getStaffById(id);
    	 map.put("id", staff.getId());
    	 map.put("name", staff.getName());
    	 map.put("phone", staff.getPhone());
    	 map.put("email", staff.getEmail());
    	 map.put("adress", staff.getAdress());
    	 map.put("Gender", staff.getGender());
    	 map.put("salary", staff.getSalary());
    	
    	 return "Staffprofile";
     }
     @RequestMapping(value = "/deletestaff/{id}", method = RequestMethod.GET)
     public String deleteStaff(@PathVariable("id")Integer id, Map<String, Object> map){
    	 Staff staff=StaffService.getStaffById(id);
         StaffService.delete(staff);    	
    	 return "redirect:/stafflist.html";
     }
     

   

}
