package com.mahfuj.security;

public interface ISecurityUserService {

    String validatePasswordResetToken(long id, String token);

}
