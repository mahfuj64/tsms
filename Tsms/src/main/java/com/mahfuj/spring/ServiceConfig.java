package com.mahfuj.spring;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan({ "com.mahfuj.service" })
public class ServiceConfig {
}
